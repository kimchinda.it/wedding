
package panel;

import java.awt.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.sql.*;
import Connection.ConnectionDB;
import PlugIn.MyInput;
import PlugIn.SubDefaultTableModel;
import java.awt.Font;
import resources.Component;
/**
 *
 * @author chinda
 */
public class ReturMoney extends javax.swing.JPanel {

    /**
     * Creates new form ReturMoney
     */
    public ReturMoney() {
        initComponents();
        com = new Component();
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            datPacker.setDate(sdf.parse(sdf.format(System.currentTimeMillis())));
            modelGuest = new SubDefaultTableModel(null,new Object[]{com.getRb().getString("lbGuestId"),com.getRb().getString("lbName"),com.getRb().getString("lbGender"),
               com.getRb().getString("lbVillage"),com.getRb().getString("lbOther")});
            modelGuestDetail = new SubDefaultTableModel(null,new Object[]{com.getRb().getString("lbNo"),com.getRb().getString("lbGuestId"),com.getRb().getString("lbRiel"),
                com.getRb().getString("lbDollar"),com.getRb().getString("lbDong"),com.getRb().getString("lbDate")});
            tblGuest.setModel(modelGuest);
            tblGuestDetail.setModel(modelGuestDetail);
            getDatabase("select id,name,gender,village,other from tblguest",modelGuest);
            detailId = getDatabase("select * from tblguestdetail", modelGuestDetail);
            com.setButton(new String[]{"btnNew","btnAdd","btnDelete"}, btnNew,btnAdd,btnDelete);
            com.setLable(new String[]{"lbGuestId","lbName","lbDong","lbDollar","lbRiel","lbDate","lbSearchName","lbSearchId"}, lbNo,lbName,lbDong,lbDollar,lbRiel,lbDate,lbSearchName,lbSearchId);
            com.setPanel(new String[]{"panelAdd","panelInfor"}, panelAdd,panelInfor);
            lbReturnMoneyTitle.setText(com.getRb().getString("lbReturnMoneyTitle"));
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbReturnMoneyTitle = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        panelAdd = new javax.swing.JPanel();
        lbNo = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        lbRiel = new javax.swing.JLabel();
        txtRiel = new javax.swing.JTextField();
        lbDollar = new javax.swing.JLabel();
        txtDollar = new javax.swing.JTextField();
        lbDong = new javax.swing.JLabel();
        txtDong = new javax.swing.JTextField();
        lbDate = new javax.swing.JLabel();
        btnNew = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        lbName = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        lbSearchName = new javax.swing.JLabel();
        txtSearchName = new javax.swing.JTextField();
        datPacker = new com.toedter.calendar.JDateChooser();
        scTableDetail1 = new javax.swing.JScrollPane();
        tblGuest = new PlugIn.SubJTable();
        panelInfor = new javax.swing.JPanel();
        scTableDetail = new javax.swing.JScrollPane();
        tblGuestDetail = new PlugIn.SubJTable();
        lbSearchId = new javax.swing.JLabel();
        txtSearchId = new javax.swing.JTextField();

        setBackground(java.awt.Color.white);

        lbReturnMoneyTitle.setFont(new java.awt.Font("Khmer OS Battambang", 1, 24)); // NOI18N
        lbReturnMoneyTitle.setForeground(new java.awt.Color(204, 0, 0));
        lbReturnMoneyTitle.setText("សងចំណងដៃ");

        jSeparator1.setForeground(new java.awt.Color(0, 255, 0));

        panelAdd.setBackground(java.awt.Color.white);
        panelAdd.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "បន្ថែម", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Khmer OS Battambang", 1, 12))); // NOI18N
        panelAdd.setPreferredSize(new java.awt.Dimension(614, 284));

        lbNo.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbNo.setText("លេខសម្គាល់");

        txtId.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtId.setFocusable(false);

        lbRiel.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbRiel.setText("លុយរៀល");

        txtRiel.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtRiel.setText("0.00");
        txtRiel.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtRielFocusGained(evt);
            }
        });
        txtRiel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRielKeyTyped(evt);
            }
        });

        lbDollar.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbDollar.setText("លុយដុល្លារ");

        txtDollar.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtDollar.setText("0.00");
        txtDollar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtDollarFocusGained(evt);
            }
        });
        txtDollar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDollarKeyTyped(evt);
            }
        });

        lbDong.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbDong.setText("លុយដុង");

        txtDong.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtDong.setText("0.00");
        txtDong.setName(""); // NOI18N
        txtDong.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtDongFocusGained(evt);
            }
        });
        txtDong.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDongKeyTyped(evt);
            }
        });

        lbDate.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbDate.setText("កាលបរិច្ឆេទ");

        btnNew.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        btnNew.setText("ថ្មី");
        btnNew.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNewMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNewMouseExited(evt);
            }
        });
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        btnAdd.setText("រក្សាទុក");
        btnAdd.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddMouseExited(evt);
            }
        });
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        btnDelete.setText("លុប");
        btnDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDeleteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDeleteMouseExited(evt);
            }
        });
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        lbName.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbName.setText("ឈ្មោះ");

        txtName.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N

        lbSearchName.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbSearchName.setText("ស្វែងរកឈ្មោះ");

        txtSearchName.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtSearchName.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtSearchNameCaretUpdate(evt);
            }
        });

        datPacker.setDateFormatString("yyyy/MM/dd");
        datPacker.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N

        tblGuest.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblGuest.setColorSelected(new java.awt.Color(51, 204, 255));
        tblGuest.setFont(new java.awt.Font("Khmer OS Battambang", 0, 12)); // NOI18N
        tblGuest.setRowHeight(30);
        tblGuest.getTableHeader().setResizingAllowed(false);
        tblGuest.getTableHeader().setReorderingAllowed(false);
        tblGuest.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblGuestMouseClicked(evt);
            }
        });
        tblGuest.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblGuestKeyPressed(evt);
            }
        });
        scTableDetail1.setViewportView(tblGuest);
        tblGuestDetail.setFontHeader(new Font("Khmer OS Battambang",Font.BOLD,12));
        tblGuestDetail.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        javax.swing.GroupLayout panelAddLayout = new javax.swing.GroupLayout(panelAdd);
        panelAdd.setLayout(panelAddLayout);
        panelAddLayout.setHorizontalGroup(
            panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAddLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addComponent(lbNo, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55)
                        .addComponent(lbName, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addComponent(lbDong, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(txtDong, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55)
                        .addComponent(lbRiel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(txtRiel, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addComponent(lbDate, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(datPacker, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55)
                        .addComponent(lbDollar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(txtDollar, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addComponent(lbSearchName, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(txtSearchName, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55)
                        .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(scTableDetail1, javax.swing.GroupLayout.PREFERRED_SIZE, 636, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15))
        );
        panelAddLayout.setVerticalGroup(
            panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAddLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(lbNo))
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(lbName))
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lbDong))
                    .addComponent(txtDong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(lbRiel))
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(txtRiel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(lbDate))
                    .addComponent(datPacker, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lbDollar))
                    .addComponent(txtDollar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(lbSearchName))
                    .addComponent(txtSearchName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(btnNew))
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAdd)
                            .addComponent(btnDelete))))
                .addGap(12, 12, 12)
                .addComponent(scTableDetail1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(6, 6, 6))
        );

        panelInfor.setBackground(java.awt.Color.white);
        panelInfor.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ពត៍មាន", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Khmer OS Battambang", 1, 12))); // NOI18N

        tblGuestDetail.setColorSelected(new java.awt.Color(51, 204, 255));
        tblGuestDetail.setFont(new java.awt.Font("Khmer OS Battambang", 0, 12)); // NOI18N
        tblGuestDetail.setRowHeight(30);
        tblGuestDetail.getTableHeader().setResizingAllowed(false);
        tblGuestDetail.getTableHeader().setReorderingAllowed(false);
        tblGuestDetail.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblGuestDetailMouseClicked(evt);
            }
        });
        tblGuestDetail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblGuestDetailKeyPressed(evt);
            }
        });
        scTableDetail.setViewportView(tblGuestDetail);
        tblGuest.setFontHeader(new Font("Khmer OS Battambang",Font.BOLD,12));
        tblGuest.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        lbSearchId.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbSearchId.setText("ស្វែងរកលេខសម្គាល់");

        txtSearchId.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtSearchId.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtSearchIdCaretUpdate(evt);
            }
        });
        txtSearchId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSearchIdKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout panelInforLayout = new javax.swing.GroupLayout(panelInfor);
        panelInfor.setLayout(panelInforLayout);
        panelInforLayout.setHorizontalGroup(
            panelInforLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInforLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelInforLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scTableDetail)
                    .addGroup(panelInforLayout.createSequentialGroup()
                        .addComponent(lbSearchId, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(txtSearchId, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelInforLayout.setVerticalGroup(
            panelInforLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelInforLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panelInforLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbSearchId)
                    .addComponent(txtSearchId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addComponent(scTableDetail, javax.swing.GroupLayout.DEFAULT_SIZE, 411, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbReturnMoneyTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(panelAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 667, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelInfor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(16, 16, 16))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbReturnMoneyTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelInfor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelAdd, javax.swing.GroupLayout.DEFAULT_SIZE, 516, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnNewMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseEntered
        btnNew.setBackground(Color.PINK);
        btnNew.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnNewMouseEntered

    private void btnNewMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseExited
        btnNew.setBackground(Color.white);
        btnNew.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnNewMouseExited

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        btnNew();
        tblGuest.clearSelection();
        tblGuestDetail.clearSelection();

    }//GEN-LAST:event_btnNewActionPerformed
    private void btnNew(){
        clearTextField(txtRiel,txtDong,txtDollar);
        txtId.setText("");
        txtName.setText("");
        btnAdd.setText("រក្សាទុក");
        btnAdd.setBackground(new Color(214,217,223));
        btnAdd.setForeground(new Color(0,0,0));
    }
    private void btnAddMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddMouseEntered
        if(!btnAdd.getText().equals("កែប្រែ")){
            btnAdd.setBackground(Color.PINK);
            btnAdd.setForeground(Color.BLACK);
        }else{
            btnAdd.setBackground(Color.green);
            btnAdd.setForeground(Color.black);
        }
        //[255,0,22]   11
    }//GEN-LAST:event_btnAddMouseEntered

    private void btnAddMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddMouseExited
        if(!btnAdd.getText().equals("កែប្រែ")){
            btnAdd.setBackground(Color.white);
            btnAdd.setForeground(Color.BLACK);
        }else{
            btnAdd.setBackground(Color.red);
            btnAdd.setForeground(Color.black);
        }
    }//GEN-LAST:event_btnAddMouseExited

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        String btn = btnAdd.getText();
        try{
            if(checkTextField(txtId)){
                int id =Integer.parseInt(txtId.getText());
                double riel = Double.parseDouble(txtRiel.getText());
                double dollar = Double.parseDouble(txtDollar.getText());
                double dong = Double.parseDouble(txtDong.getText());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String d = sdf.format(datPacker.getDate());
                String sql="";
                Object obj[] ={detailId+1,id,riel,dollar,dong,d};
                if(!btn.equals(com.getRb().getString("btnUpdate"))){
                    sql = "insert into tblguestdetail (id,reil,dollar,dong,date) values("
                    + id+","+riel+","+dollar+","+dong+",'"+d+"')";
                     setDatabase(sql);
                    modelGuestDetail.addRow(obj);
                    detailId++;
                    JOptionPane.showMessageDialog(this, com.getRb().getString("msgSave"));
                    int max = scTableDetail.getVerticalScrollBar().getMaximum();
                    scTableDetail.getVerticalScrollBar().setValue(max+1);
                }else{
                    
                    int click = JOptionPane.showConfirmDialog(this, com.getRb().getString("updateInfor"), com.getRb().getString("btnUpdate"), JOptionPane.YES_NO_OPTION);
                    if(click ==JOptionPane.YES_OPTION){
                        int index =tblGuestDetail.getSelectedRow();
                        int detailId = Integer.parseInt(tblGuestDetail.getValueAt(index, 0)+"");
                        obj[0] = detailId;
                        sql = "update tblguestdetail set id = "+id+",reil = "+riel
                        +",dollar = "+dollar+",dong = "+dong+",date = '"+d+"' where guestDetailId = "+detailId;
                        setDatabase(sql);
                        for(int i=0; i<obj.length;i++)
                            modelGuestDetail.setValueAt(obj[i], index, i);
                        JOptionPane.showMessageDialog(this, com.getRb().getString("msgUpdate"));
                    }
                }
            }
        }catch(Exception e){
            String msg;
              if(btn.equals(com.getRb().getString("btnUpdate")))
                  msg = com.getRb().getString("update");
              else
                  msg = com.getRb().getString("insert");
              JOptionPane.showMessageDialog(this, ConnectionDB.getUserName()+" "+msg,"Error",JOptionPane.ERROR_MESSAGE);
        }
        btnNewActionPerformed(evt);
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseEntered
        btnDelete.setBackground(Color.PINK);
        btnDelete.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnDeleteMouseEntered

    private void btnDeleteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseExited
        btnDelete.setBackground(Color.white);
        btnDelete.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnDeleteMouseExited

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        try{
            if(!tblGuestDetail.getSelectionModel().isSelectionEmpty()){
                int index[] = tblGuestDetail.getSelectedRows();
                int id[] = new int[index.length];
                int count =0;
                String message="";
                for(int i=0; i<index.length;i++){
                    id[i] = Integer.parseInt(tblGuestDetail.getValueAt(index[i], 0)+"");
                    count++;
                }
                if(count==1)
                   message = "Are you sure you want to delete a record?";
                else
                   message = "Are you sure you want to delete "+count+" records?";
                int click = JOptionPane.showConfirmDialog(this, message, "Comfirm delete", JOptionPane.YES_NO_OPTION);
                String sql = "";
                if(click == JOptionPane.YES_OPTION){
                    for(int i=0; i<id.length; i++){
                        sql = "delete from tblguestdetail where guestDetailId = "+id[i];
                        setDatabase(sql);
                    }
                    modelGuestDetail.removeSelectedValues(tblGuestDetail);
                }
            }   
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, ConnectionDB.getUserName()+" "+com.getRb().getString("delete"),"Error",JOptionPane.ERROR_MESSAGE);
        }
        btnNewActionPerformed(evt);
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void txtSearchNameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtSearchNameCaretUpdate
        String sql = "";
        if(!txtSearchName.getText().equals(""))
        sql = "select id,name,gender,village,other from tblguest where name like '"+txtSearchName.getText()+"%'";
        else
        sql = "select id,name,gender,village,other from tblguest";
        getDatabase(sql, modelGuest);
    }//GEN-LAST:event_txtSearchNameCaretUpdate

    private void txtDongFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDongFocusGained
        txtDong.selectAll();
    }//GEN-LAST:event_txtDongFocusGained

    private void txtRielFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtRielFocusGained
        txtRiel.selectAll();
    }//GEN-LAST:event_txtRielFocusGained

    private void txtDollarFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDollarFocusGained
        txtDollar.selectAll();
    }//GEN-LAST:event_txtDollarFocusGained

    private void tblGuestDetailKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblGuestDetailKeyPressed
        int index = tblGuestDetail.getSelectedRow();
        if(evt.getKeyCode()==40)
            index++;
        else if(evt.getKeyCode()==38)
            index--;
        selectionRow(index);
    }//GEN-LAST:event_tblGuestDetailKeyPressed

    private void tblGuestDetailMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblGuestDetailMouseClicked
        btnNew();
        int index = tblGuestDetail.getSelectedRow();
        selectionRow(index);
        btnAdd.setText(com.getRb().getString("btnUpdate"));
        btnAdd.setBackground(Color.red);
        btnAdd.setForeground(Color.white);
    }//GEN-LAST:event_tblGuestDetailMouseClicked

    private void tblGuestMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblGuestMouseClicked
        btnNew();
        int index = tblGuest.getSelectedRow();
        txtId.setText(tblGuest.getValueAt(index, 0)+"");
        txtName.setText(tblGuest.getValueAt(index, 1)+"");
        tblGuestDetail.clearSelection();
    }//GEN-LAST:event_tblGuestMouseClicked

    private void tblGuestKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblGuestKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tblGuestKeyPressed

    private void txtSearchIdCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtSearchIdCaretUpdate
        String sql;
        if(!txtSearchId.getText().equals(""))
            sql = "select * from tblguestdetail where id ="+Integer.parseInt(txtSearchId.getText());
        else
            sql = "select * from tblguestdetail";
        getDatabase(sql, modelGuestDetail);
    }//GEN-LAST:event_txtSearchIdCaretUpdate

    private void txtSearchIdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchIdKeyTyped
        MyInput.inputInteger(evt);
    }//GEN-LAST:event_txtSearchIdKeyTyped

    private void txtDongKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDongKeyTyped
        MyInput.inputFloat(evt, txtDong);
    }//GEN-LAST:event_txtDongKeyTyped

    private void txtRielKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRielKeyTyped
        MyInput.inputFloat(evt, txtRiel);
    }//GEN-LAST:event_txtRielKeyTyped

    private void txtDollarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDollarKeyTyped
        MyInput.inputFloat(evt, txtDollar);
    }//GEN-LAST:event_txtDollarKeyTyped
    private void setDatabase(String sql)throws Exception{
        Statement s = ConnectionDB.getDataCon().createStatement(1004, 1008);
        s.execute(sql);
        s.close();
    }
    private int getDatabase(String sql,SubDefaultTableModel model){
        int id = 0;
        try {
            model.removeAllRows();
            Statement s = ConnectionDB.getDataCon().createStatement(1004, 1008);
            ResultSet rs = s.executeQuery(sql);
            ResultSetMetaData rm = rs.getMetaData();
            //int header[] = new ingint[rm.getColumnCount()];
            String row[] = new String[rm.getColumnCount()];
            while(rs.next()){
                id = rs.getInt(1);
                for(int i=0; i<row.length; i++)
                    row[i] = rs.getString(i+1);
                model.addRow(row);
            }
            rs.close();
            s.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
        return id;
    }
    private void selectionRow(int index){
        try {
             DecimalFormat df = new DecimalFormat("#,##0.00");
            int id = Integer.parseInt(tblGuestDetail.getValueAt(index, 1)+"");
            txtId.setText(id+"");
            txtRiel.setText(df.parse(tblGuestDetail.getValueAt(index, 2)+"")+"");
            txtDollar.setText(df.parse(tblGuestDetail.getValueAt(index, 3)+"")+"");
            txtDong.setText(df.parse(tblGuestDetail.getValueAt(index, 4)+"")+"");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date d = sdf.parse(tblGuestDetail.getValueAt(index, 5)+"");
            datPacker.setDate(d);
            String sql = "select name from tblguest where id = "+id;
            Statement s = ConnectionDB.getDataCon().createStatement(1004,1008);
            ResultSet rs = s.executeQuery(sql);
            if(rs.first())
                txtName.setText(rs.getString(1));
            rs.close();
            s.close();
        } catch (Exception e) {
        }
    }
    private boolean checkTextField(JTextField ...txt){
        for(JTextField temp:txt){
            String str = temp.getText();
            if(str.equals("")){
                temp.requestFocus();
                return false;
            }
        }
        return true;
    }
    private void clearTextField(JTextField ...txt){
        for(JTextField temp: txt)
            temp.setText("0.00");
        txt[0].requestFocus();
    }
    private Component com;
    private int detailId;
    private SubDefaultTableModel modelGuest,modelGuestDetail;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnNew;
    private com.toedter.calendar.JDateChooser datPacker;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbDate;
    private javax.swing.JLabel lbDollar;
    private javax.swing.JLabel lbDong;
    private javax.swing.JLabel lbName;
    private javax.swing.JLabel lbNo;
    private javax.swing.JLabel lbReturnMoneyTitle;
    private javax.swing.JLabel lbRiel;
    private javax.swing.JLabel lbSearchId;
    private javax.swing.JLabel lbSearchName;
    private javax.swing.JPanel panelAdd;
    private javax.swing.JPanel panelInfor;
    private javax.swing.JScrollPane scTableDetail;
    private javax.swing.JScrollPane scTableDetail1;
    private PlugIn.SubJTable tblGuest;
    private PlugIn.SubJTable tblGuestDetail;
    private javax.swing.JTextField txtDollar;
    private javax.swing.JTextField txtDong;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtRiel;
    private javax.swing.JTextField txtSearchId;
    private javax.swing.JTextField txtSearchName;
    // End of variables declaration//GEN-END:variables
}

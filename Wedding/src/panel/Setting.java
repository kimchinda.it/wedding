
package panel;

import Connection.ConnectionDB;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import main.LogIn;
import resources.Component;
import resources.Temp;

/**
 *
 * @author chinda
 */
public class Setting extends javax.swing.JPanel {

    /**
     * Creates new form Test
     */
    public Setting() {
        initComponents();
        com = new Component();
        checkCombo(com.getLanguage());
        String []text = {"btnOk","btnReset"};
        JButton btn[] = {btnOk,btnReset};
        com.setButton(text, btn);
        lbLanguage.setText(com.getRb().getString("lbLanguage"));
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cmbLanguage = new javax.swing.JComboBox<>();
        btnOk = new javax.swing.JButton();
        lbLanguage = new javax.swing.JLabel();
        lbDarkMode = new javax.swing.JLabel();
        btnReset = new javax.swing.JButton();
        lbTest = new javax.swing.JLabel();

        cmbLanguage.setFont(new java.awt.Font("Khmer OS Battambang", 1, 14)); // NOI18N
        cmbLanguage.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "English", "Khmer" }));

        btnOk.setFont(new java.awt.Font("Khmer OS Battambang", 1, 14)); // NOI18N
        btnOk.setText("យល់ព្រម");
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        lbLanguage.setFont(new java.awt.Font("Khmer OS Battambang", 1, 14)); // NOI18N
        lbLanguage.setText("ភាសា");

        lbDarkMode.setFont(new java.awt.Font("Khmer OS Battambang", 1, 15)); // NOI18N
        lbDarkMode.setText("Night Mode");

        btnReset.setFont(new java.awt.Font("Khmer OS Battambang", 1, 14)); // NOI18N
        btnReset.setText("កំណត់ឡើវិញ");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        lbTest.setFont(new java.awt.Font("Khmer OS Battambang", 1, 15)); // NOI18N
        lbTest.setText("Night Mode");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbDarkMode, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnReset)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnOk))
                            .addComponent(lbTest, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbLanguage, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cmbLanguage, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(96, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbLanguage)
                    .addComponent(cmbLanguage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbDarkMode)
                    .addComponent(lbTest))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOk)
                    .addComponent(btnReset))
                .addContainerGap(99, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        setLanguage(cmbLanguage.getSelectedItem().toString());
    }//GEN-LAST:event_btnOkActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed
        setLanguage("Khmer");
    }//GEN-LAST:event_btnResetActionPerformed
    private void checkCombo(String lang){
        for(int i=0; i<cmbLanguage.getItemCount(); i++){
            if(lang.equals(cmbLanguage.getItemAt(i))){
                cmbLanguage.setSelectedIndex(i);
                
            }
        }
    }
    private void setLanguage(String language){
        try{
            com.setLanguage(language);
            int option = JOptionPane.showConfirmDialog(this, com.getRb().getString("information"),
                    com.getRb().getString("changeLanguage"),JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_OPTION){
                Temp.THIS.dispose();
                ConnectionDB.getDataCon().close();
                LogIn.main(null);
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }    
    private Component com;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnReset;
    private javax.swing.JComboBox<String> cmbLanguage;
    private javax.swing.JLabel lbDarkMode;
    private javax.swing.JLabel lbLanguage;
    private javax.swing.JLabel lbTest;
    // End of variables declaration//GEN-END:variables
}

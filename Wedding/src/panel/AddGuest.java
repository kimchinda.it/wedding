
package panel;

import Connection.ConnectionDB;
import PlugIn.SubDefaultTableModel;
import java.sql.*;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import resources.Component;


/**
 *
 * @author CHINDA
 */
public class AddGuest extends javax.swing.JPanel {

    public AddGuest() {
        initComponents();
        com = new Component();
        model = new SubDefaultTableModel(null,new Object[]{com.getRb().getString("lbGuestId"),com.getRb().getString("lbName"),
                com.getRb().getString("lbGender"),com.getRb().getString("lbVillage"),com.getRb().getString("lbCommune"),
                com.getRb().getString("lbCity"),com.getRb().getString("lbProvince"),com.getRb().getString("lbOther")});
        tbl.setModel(model);
        tbl.setColumnWidth(0, 40);
        tbl.setColumnWidth(1, 90);
        tbl.setColumnWidth(2, 30);
        getDatabase("select * from tblguest", model);
        tempId=++id;
        txtId.setText(id+"");
//        panelAdd.setBorder(javax.swing.BorderFactory.createTitledBorder(null, com.getRb().getString("panelAdd"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Khmer OS Battambang", 1, 12))); // NOI18N
//        panelInfor.setBorder(javax.swing.BorderFactory.createTitledBorder(null, com.getRb().getString("panelInfor"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Khmer OS Battambang", 1, 12))); // NOI18N
        com.setPanel(new String[]{"panelAdd","panelInfor"}, new JPanel[]{panelAdd,panelInfor});
        String []text = {"lbGuestId","lbName","lbGender","lbVillage","lbCommune","lbCity","lbProvince","lbOther","lbSearchName","lbGuestTitel"};
        JLabel []lb = {lbNo,lbName,lbGender,lbViilage,lbCommune,lbCity,lbProvince,lbOther,lbSearchName,lbGuestTitle};
        com.setLable(text, lb);
        com.setButton(new String[]{"btnNew","btnAdd","btnDelete"}, btnNew,btnAdd,btnDelete);
        
   
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbGuestTitle = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        panelAdd = new javax.swing.JPanel();
        lbNo = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        lbGender = new javax.swing.JLabel();
        comboGender = new javax.swing.JComboBox<>();
        lbViilage = new javax.swing.JLabel();
        txtVillage = new javax.swing.JTextField();
        lbCity = new javax.swing.JLabel();
        txtCity = new javax.swing.JTextField();
        lbCommune = new javax.swing.JLabel();
        txtComminue = new javax.swing.JTextField();
        lbOther = new javax.swing.JLabel();
        txtOther = new javax.swing.JTextField();
        lbProvince = new javax.swing.JLabel();
        txtProvince = new javax.swing.JTextField();
        btnNew = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        lbName = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        panelInfor = new javax.swing.JPanel();
        lbSearchName = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        scTable = new javax.swing.JScrollPane();
        tbl = new PlugIn.SubJTable();

        setBackground(java.awt.Color.white);

        lbGuestTitle.setFont(new java.awt.Font("Khmer OS Battambang", 1, 24)); // NOI18N
        lbGuestTitle.setForeground(new java.awt.Color(204, 0, 0));
        lbGuestTitle.setText("បន្ថែមពត៌មានរបស់ភ្ញៀវ");

        jSeparator1.setBackground(new java.awt.Color(204, 0, 0));
        jSeparator1.setForeground(new java.awt.Color(0, 255, 0));

        panelAdd.setBackground(java.awt.Color.white);
        panelAdd.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "បន្ថែម", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Khmer OS Battambang", 1, 12))); // NOI18N

        lbNo.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbNo.setText("លេខសម្គាល់");

        txtId.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtId.setFocusable(false);

        lbGender.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbGender.setText("ភេទ");

        comboGender.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        comboGender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "គ្មាន", "ប្រុស", "ស្រី" }));

        lbViilage.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbViilage.setText("ភូមិ");

        txtVillage.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtVillage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtVillageActionPerformed(evt);
            }
        });

        lbCity.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbCity.setText("ស្រុក/ក្រុង");

        txtCity.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtCity.setText("បាវិត");
        txtCity.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtCityFocusGained(evt);
            }
        });

        lbCommune.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbCommune.setText("ឃុំ/សង្កាត់");

        txtComminue.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtComminue.setText("បាវិត");
        txtComminue.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtComminueFocusGained(evt);
            }
        });

        lbOther.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbOther.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbOther.setText("ផ្សេង");

        txtOther.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N

        lbProvince.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbProvince.setText("ខេត្ត");

        txtProvince.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtProvince.setText("ស្វាយរៀង");

        btnNew.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        btnNew.setText("ថ្មី");
        btnNew.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNewMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNewMouseExited(evt);
            }
        });
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        btnAdd.setText("រក្សាទុក");
        btnAdd.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAddMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAddMouseExited(evt);
            }
        });
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        btnDelete.setText("លុប");
        btnDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDeleteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDeleteMouseExited(evt);
            }
        });
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        lbName.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbName.setText("ឈ្មោះ");

        txtName.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N

        javax.swing.GroupLayout panelAddLayout = new javax.swing.GroupLayout(panelAdd);
        panelAdd.setLayout(panelAddLayout);
        panelAddLayout.setHorizontalGroup(
            panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAddLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(lbNo, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(lbName, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelAddLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(lbGender, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(comboGender, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(lbViilage, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(txtVillage, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelAddLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(lbCommune, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(txtComminue, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(lbCity, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelAddLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(lbProvince, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(txtProvince, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(lbOther, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(txtOther, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(panelAddLayout.createSequentialGroup()
                .addGap(280, 280, 280)
                .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panelAddLayout.setVerticalGroup(
            panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAddLayout.createSequentialGroup()
                .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbNo)
                            .addComponent(lbName))))
                .addGap(20, 20, 20)
                .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtVillage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbGender)
                            .addComponent(lbViilage))))
                .addGap(20, 20, 20)
                .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtComminue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbCommune)
                            .addComponent(lbCity))))
                .addGap(19, 19, 19)
                .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lbProvince))
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(txtProvince, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelAddLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lbOther))
                    .addComponent(txtOther, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(panelAddLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnNew)
                    .addComponent(btnAdd)
                    .addComponent(btnDelete)))
        );

        panelInfor.setBackground(java.awt.Color.white);
        panelInfor.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ពត៌មាន", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Khmer OS Battambang", 1, 12))); // NOI18N

        lbSearchName.setFont(new java.awt.Font("Khmer OS Battambang", 1, 14)); // NOI18N
        lbSearchName.setText("ស្វែងរកឈ្មោះ");

        txtSearch.setFont(new java.awt.Font("Khmer OS Battambang", 1, 14)); // NOI18N
        txtSearch.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtSearchCaretUpdate(evt);
            }
        });

        tbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbl.setColorSelected(new java.awt.Color(51, 204, 255));
        tbl.setFont(new java.awt.Font("Khmer OS Battambang", 0, 12)); // NOI18N
        tbl.setRowHeight(30);
        tbl.getTableHeader().setResizingAllowed(false);
        tbl.getTableHeader().setReorderingAllowed(false);
        tbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblMouseClicked(evt);
            }
        });
        tbl.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tblKeyPressed(evt);
            }
        });
        scTable.setViewportView(tbl);
        tbl.setFontHeader(new Font("Khmer OS Battambang",Font.BOLD,12));
        tbl.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        javax.swing.GroupLayout panelInforLayout = new javax.swing.GroupLayout(panelInfor);
        panelInfor.setLayout(panelInforLayout);
        panelInforLayout.setHorizontalGroup(
            panelInforLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInforLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelInforLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scTable, javax.swing.GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
                    .addGroup(panelInforLayout.createSequentialGroup()
                        .addComponent(lbSearchName, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelInforLayout.setVerticalGroup(
            panelInforLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelInforLayout.createSequentialGroup()
                .addGroup(panelInforLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbSearchName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scTable, javax.swing.GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
                        .addComponent(lbGuestTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelInfor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(16, 16, 16))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbGuestTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelInfor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        String btn = btnAdd.getText();
        try{
        if(checkTextField(txtName)){
            int gId = Integer.parseInt(txtId.getText());
            String name = txtName.getText();
            String gender = comboGender.getSelectedItem()+"";
            String village = txtVillage.getText();
            String comminue = txtComminue.getText();
            String city = txtCity.getText();
            String province = txtProvince.getText();
            String other = txtOther.getText();
            String sql = "";
            Object str[] = {gId,name,gender,village,comminue,city,province,other};
            if(!btn.equals(com.getRb().getString("btnUpdate"))){
                sql = "insert into tblguest values("+gId+",'"+name+"','"+gender+"','"+village+"','"+comminue+"','"+
                        city+"','"+province+"','"+other+"')";
                setDatabase(sql);
                model.addRow(str);
                JOptionPane.showMessageDialog(this, com.getRb().getString("msgSave"),com.getRb().getString("btnAdd"),JOptionPane.INFORMATION_MESSAGE);
                tempId=id = ++gId;
                int max = scTable.getVerticalScrollBar().getMaximum();
                 scTable.getVerticalScrollBar().setValue(max+1);
            }else{
                int click = JOptionPane.showConfirmDialog(this, com.getRb().getString("updateInfor"), com.getRb().getString("btnUpdate"), JOptionPane.YES_NO_OPTION);
                if(click == JOptionPane.YES_OPTION){
                    int index = tbl.getSelectedRow();
                    sql = "update tblguest set name ='"+name+"',gender = '"+gender+"',village ='"+village+"',comminue ="+
                               "'"+comminue+"',city = '"+city+"',province ='"+province+"',other = '"+other+"' where id ="+gId;
                    setDatabase(sql);
                    for(int i=0; i<str.length; i++)
                        model.setValueAt(str[i], index, i);
                    JOptionPane.showMessageDialog(this, com.getRb().getString("msgUpdate"),com.getRb().getString("btnUpdate"),JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }  
        }catch(Exception e){
            String msg;
            if(btn.equals(com.getRb().getString("btnUpdate")))
                msg = com.getRb().getString("update");
            else
                msg = com.getRb().getString("insert");
            JOptionPane.showMessageDialog(this, ConnectionDB.getUserName()+" "+msg,"Error",JOptionPane.ERROR_MESSAGE);
        }
         btnNewActionPerformed(evt);
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
         txtId.setText(tempId+"");
         btnAdd.setText(com.getRb().getString("btnAdd"));
         clearTextField(txtName,txtVillage,txtOther);  
         tbl.getSelectionModel().clearSelection();
         btnAdd.setBackground(new Color(255,255,255));
         btnAdd.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnNewActionPerformed
    
    private void txtSearchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtSearchCaretUpdate
        String sql = "";
        if(!txtSearch.getText().equals(""))
           sql = "select * from tblguest where name like '"+txtSearch.getText()+"%'";
       else
           sql = "select * from tblguest";
        getDatabase(sql, model);
        
    }//GEN-LAST:event_txtSearchCaretUpdate

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        try {
            int index[] = tbl.getSelectedRows();//index element{1 ,2, 4} value(3,5,1)= 3 length
            int id[] = new int[index.length];
            int count = 0;
            String sql = "",message="";
            for(int i=0; i<index.length; i++){
                id[i] = Integer.parseInt(tbl.getValueAt(index[i], 0).toString());
                count++;
            }
            if(count==1)
                message = "Are you sure you want to delete a record?";
            else if(count>1)
                message = "Are you sure you want to delete "+count+" records?";
            if(count>0){
                int click = JOptionPane.showConfirmDialog(this, message, com.getRb().getString("btnDelete"), JOptionPane.YES_NO_OPTION);
                if(click ==JOptionPane.YES_OPTION){
                    for(int i=0; i<id.length; i++){
                        sql = "delete from tblguest where id = "+id[i];
                        setDatabase(sql);
                    }
                    model.removeSelectedValues(tbl);
                    JOptionPane.showMessageDialog(this, com.getRb().getString("msgDelete"));
                }
                
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, ConnectionDB.getUserName()+" "+com.getRb().getString("delete"),"Error",JOptionPane.ERROR_MESSAGE);
            
        }
        btnNewActionPerformed(evt);
    }//GEN-LAST:event_btnDeleteActionPerformed
   
    private void btnNewMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseEntered
        btnNew.setBackground(Color.PINK);
        btnNew.setForeground(Color.BLACK);
        
    }//GEN-LAST:event_btnNewMouseEntered

    private void btnNewMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseExited
        btnNew.setBackground(Color.white);
        btnNew.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnNewMouseExited

    private void btnAddMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddMouseEntered
        if(!btnAdd.getText().equals(com.getRb().getString("btnUpdate"))){
            btnAdd.setBackground(Color.PINK);
            btnAdd.setForeground(Color.BLACK);
        }else{
            btnAdd.setBackground(Color.green);
            btnAdd.setForeground(Color.black);
        }
        
        //[255,0,22]
    }//GEN-LAST:event_btnAddMouseEntered

    private void btnAddMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddMouseExited
        if(!btnAdd.getText().equals(com.getRb().getString("btnUpdate"))){
            btnAdd.setBackground(Color.white);
            btnAdd.setForeground(Color.BLACK);
        }else{
            btnAdd.setBackground(Color.red);
            btnAdd.setForeground(Color.black);
        }
    }//GEN-LAST:event_btnAddMouseExited

    private void btnDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseEntered
         btnDelete.setBackground(Color.PINK);
         btnDelete.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnDeleteMouseEntered

    private void btnDeleteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseExited
        btnDelete.setBackground(Color.white);
        btnDelete.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnDeleteMouseExited

    private void txtVillageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtVillageActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVillageActionPerformed

    private void txtComminueFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtComminueFocusGained
        txtComminue.requestFocus();
        txtComminue.selectAll();
    }//GEN-LAST:event_txtComminueFocusGained

    private void txtCityFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCityFocusGained
        txtCity.requestFocus();
        txtCity.selectAll();
    }//GEN-LAST:event_txtCityFocusGained

    private void tblKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblKeyPressed
       int index = tbl.getSelectedRow();
       if(evt.getKeyCode() == 40)
           index++;
       else if(evt.getKeyCode() == 38)
           index--;
        selectedRow(index);
    }//GEN-LAST:event_tblKeyPressed

    private void tblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblMouseClicked
        int index = tbl.getSelectedRow();
        selectedRow(index);
        btnAdd.setText(com.getRb().getString("btnUpdate"));
        btnAdd.setBackground(Color.red);
        btnAdd.setForeground(Color.white);
    }//GEN-LAST:event_tblMouseClicked
    private boolean checkTextField(JTextField ...txt){
        for(JTextField temp:txt){
            String str = temp.getText();
            if(str.equals("")){
                temp.requestFocus();
                return false;
            }
        }
        return true;
    }
    private void clearTextField(JTextField ...txt){
        for(JTextField temp: txt)
            temp.setText("");
        txt[0].requestFocus();
        comboGender.setSelectedIndex(0);
    }
    
    private void setDatabase(String sql)throws Exception{
        Statement s = ConnectionDB.getDataCon().createStatement(1004,1008);
        s.execute(sql);
        s.close();
    }
    private void getDatabase(String sql,SubDefaultTableModel model){
        try {
            Statement s = ConnectionDB.getDataCon().createStatement(1004,1008);
            ResultSet r =  s.executeQuery(sql);
            model.removeAllRows();
            if(r.first())
                do{ 
                    id = r.getInt(1);
                    model.addRow(new Object[]{r.getInt(1),r.getString(2),r.getString(3),r.getString(4),r.getString(5),r.getString(6),r.getString(7),r.getString(8)});
                }while(r.next());
            r.close();
            s.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    private void selectedRow(int row){
        txtId.setText(tbl.getValueAt(row, 0)+"");
        txtName.setText(tbl.getValueAt(row, 1)+"");
        txtVillage.setText(tbl.getValueAt(row, 3)+"");
        txtCity.setText(tbl.getValueAt(row, 5)+"");
        txtComminue.setText(tbl.getValueAt(row, 4)+"");
        txtOther.setText(tbl.getValueAt(row, 7)+"");
        txtProvince.setText(tbl.getValueAt(row, 6)+"");
        String gender = tbl.getValueAt(row, 2)+"";
        for(int i=0; i<comboGender.getItemCount(); i++)
            if(gender.equals(comboGender. getItemAt(i))){
                comboGender.setSelectedIndex(i);
                break;
            }
    }
    private int id,tempId;
    private SubDefaultTableModel model;
    private Component com;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnNew;
    private javax.swing.JComboBox<String> comboGender;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbCity;
    private javax.swing.JLabel lbCommune;
    private javax.swing.JLabel lbGender;
    private javax.swing.JLabel lbGuestTitle;
    private javax.swing.JLabel lbName;
    private javax.swing.JLabel lbNo;
    private javax.swing.JLabel lbOther;
    private javax.swing.JLabel lbProvince;
    private javax.swing.JLabel lbSearchName;
    private javax.swing.JLabel lbViilage;
    private javax.swing.JPanel panelAdd;
    private javax.swing.JPanel panelInfor;
    private javax.swing.JScrollPane scTable;
    private PlugIn.SubJTable tbl;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtComminue;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtOther;
    private javax.swing.JTextField txtProvince;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtVillage;
    // End of variables declaration//GEN-END:variables
}

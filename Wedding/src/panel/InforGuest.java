
package panel;


import java.awt.Font;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import resources.Guest;
import java.sql.*;
import Connection.ConnectionDB;
import PlugIn.SubDefaultTableModel;
import PlugIn.SubJTable;
import java.awt.Cursor;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import jxl.SheetSettings;
import jxl.Workbook;
import jxl.format.*;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import resources.Component;
/**
 *
 * @author CHINDA
 */
public class InforGuest extends javax.swing.JPanel {

    public InforGuest() {
        com  = new Component();
        initComponents();
        tab.add(com.getRb().getString("general"),pGeneral);
        tab.add(com.getRb().getString("lbVillage"), pVillage);
        btnExcel.setText(com.getRb().getString("btnExcel"));
        com.setLable(new String[]{"lbSearchName","lbSearchName","lbSearchVillage","lbGuestInforTitle"}, lbSearchName,lbSearchNameQ,lbSearchVillage,lbGuestInforTitle);
        ls = new ArrayList<>();
        modelGeneral = new SubDefaultTableModel();
        modelVillage = new SubDefaultTableModel();
        modelQuery = new SubDefaultTableModel();
        setHeader(modelGeneral, tblGuest);
        setHeader(modelQuery, tblQuery);
        setHeader(modelVillage, tblVillage);
        getVillage();
        String sql = "select * from tblguest";
        getDatabase(sql,modelGeneral);  
        getDatabase(sql, modelVillage);
        getDatabase(sql, modelQuery);
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbGuestInforTitle = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        tab = new javax.swing.JTabbedPane();
        pGeneral = new javax.swing.JPanel();
        lbSearchName = new javax.swing.JLabel();
        txtSearchName = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblGuest = new PlugIn.SubJTable();
        pVillage = new javax.swing.JPanel();
        lbSearchVillage = new javax.swing.JLabel();
        btnExcel = new javax.swing.JButton();
        cmbVillage = new javax.swing.JComboBox<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblVillage = new PlugIn.SubJTable();
        pQuery = new javax.swing.JPanel();
        lbSearchNameQ = new javax.swing.JLabel();
        txtQurey = new javax.swing.JTextField();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblQuery = new PlugIn.SubJTable();

        setBackground(java.awt.Color.white);

        lbGuestInforTitle.setFont(new java.awt.Font("Khmer OS Battambang", 1, 24)); // NOI18N
        lbGuestInforTitle.setForeground(new java.awt.Color(204, 0, 0));
        lbGuestInforTitle.setText("ពត៌មានរបស់ភ្ញៀវ");

        jSeparator1.setForeground(new java.awt.Color(0, 255, 0));

        tab.setFont(new java.awt.Font("Khmer OS Battambang", 0, 12)); // NOI18N

        pGeneral.setBackground(java.awt.Color.white);

        lbSearchName.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbSearchName.setText("ស្វែងរកឈ្មោះ");

        txtSearchName.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtSearchName.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtSearchNameCaretUpdate(evt);
            }
        });

        tblGuest.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblGuest.setColorSelected(new java.awt.Color(51, 204, 255));
        tblGuest.setFont(new java.awt.Font("Khmer OS Battambang", 0, 12)); // NOI18N
        tblGuest.setRowHeight(30);
        tblGuest.getTableHeader().setResizingAllowed(false);
        tblGuest.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(tblGuest);
        tblGuest.getTableHeader().setFont(new Font("Khmer OS Battambang",Font.BOLD,12));
        tblGuest.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        javax.swing.GroupLayout pGeneralLayout = new javax.swing.GroupLayout(pGeneral);
        pGeneral.setLayout(pGeneralLayout);
        pGeneralLayout.setHorizontalGroup(
            pGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pGeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 815, Short.MAX_VALUE)
                    .addGroup(pGeneralLayout.createSequentialGroup()
                        .addComponent(lbSearchName, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtSearchName, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pGeneralLayout.setVerticalGroup(
            pGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pGeneralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbSearchName)
                    .addComponent(txtSearchName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 455, Short.MAX_VALUE)
                .addContainerGap())
        );

        tab.addTab("ទូទៅ", pGeneral);

        pVillage.setBackground(java.awt.Color.white);

        lbSearchVillage.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbSearchVillage.setText("ស្វែងរកឈ្មោះភូមិ");

        btnExcel.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        btnExcel.setText("ភ្ជាប់ទៅ Excel");
        btnExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcelActionPerformed(evt);
            }
        });

        cmbVillage.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        cmbVillage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbVillageActionPerformed(evt);
            }
        });

        tblVillage.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblVillage.setColorSelected(new java.awt.Color(51, 204, 255));
        tblVillage.setFont(new java.awt.Font("Khmer OS Battambang", 0, 12)); // NOI18N
        tblVillage.setRowHeight(30);
        tblVillage.getTableHeader().setResizingAllowed(false);
        tblVillage.getTableHeader().setReorderingAllowed(false);
        jScrollPane5.setViewportView(tblVillage);
        tblVillage.getTableHeader().setFont(new Font("Khmer OS Battambang",Font.BOLD,12));
        tblVillage.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        javax.swing.GroupLayout pVillageLayout = new javax.swing.GroupLayout(pVillage);
        pVillage.setLayout(pVillageLayout);
        pVillageLayout.setHorizontalGroup(
            pVillageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pVillageLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pVillageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pVillageLayout.createSequentialGroup()
                        .addComponent(lbSearchVillage, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cmbVillage, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 382, Short.MAX_VALUE)
                        .addComponent(btnExcel))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 815, Short.MAX_VALUE))
                .addContainerGap())
        );
        pVillageLayout.setVerticalGroup(
            pVillageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pVillageLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pVillageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pVillageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbSearchVillage)
                        .addComponent(btnExcel))
                    .addComponent(cmbVillage, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE))
        );

        tab.addTab("ភូមិ", pVillage);

        pQuery.setBackground(java.awt.Color.white);

        lbSearchNameQ.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        lbSearchNameQ.setText("ស្វែងរកឈ្មោះ");

        txtQurey.setFont(new java.awt.Font("Khmer OS Battambang", 1, 12)); // NOI18N
        txtQurey.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtQureyCaretUpdate(evt);
            }
        });

        tblQuery.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblQuery.setColorSelected(new java.awt.Color(51, 204, 255));
        tblQuery.setFont(new java.awt.Font("Khmer OS Battambang", 0, 12)); // NOI18N
        tblQuery.setRowHeight(30);
        tblQuery.getTableHeader().setResizingAllowed(false);
        tblQuery.getTableHeader().setReorderingAllowed(false);
        jScrollPane6.setViewportView(tblQuery);
        tblQuery.getTableHeader().setFont(new Font("Khmer OS Battambang",Font.BOLD,12));
        tblQuery.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        javax.swing.GroupLayout pQueryLayout = new javax.swing.GroupLayout(pQuery);
        pQuery.setLayout(pQueryLayout);
        pQueryLayout.setHorizontalGroup(
            pQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pQueryLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pQueryLayout.createSequentialGroup()
                        .addComponent(lbSearchNameQ, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtQurey, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 507, Short.MAX_VALUE))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 815, Short.MAX_VALUE))
                .addContainerGap())
        );
        pQueryLayout.setVerticalGroup(
            pQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pQueryLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbSearchNameQ)
                    .addComponent(txtQurey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 455, Short.MAX_VALUE)
                .addContainerGap())
        );

        tab.addTab("Query", pQuery);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tab)
                    .addComponent(lbGuestInforTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbGuestInforTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tab)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtSearchNameCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtSearchNameCaretUpdate
        String sql;
        if(!txtSearchName.getText().equals("")){
            sql = "select * from tblguest where name like '"+txtSearchName.getText()+"%'";
            
        }else
            sql = "select * from tblguest";
        getDatabase(sql, modelGeneral);
        
    }//GEN-LAST:event_txtSearchNameCaretUpdate
    
    private void btnExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcelActionPerformed
        try {
            String text = cmbVillage.getSelectedItem().toString();
           if(!text.isEmpty()){
               File fileName = null;
                JFileChooser fc = new JFileChooser(path);
                if(fc.showSaveDialog(this)==JFileChooser.APPROVE_OPTION){
                    fileName = fc.getSelectedFile();
                    path = fileName.getAbsolutePath();
                }
                WritableWorkbook wb = Workbook.createWorkbook(fileName);
                WritableSheet sheet = wb.createSheet(text, 1);
                SheetSettings setting = sheet.getSettings();
                setting.setTopMargin(1.5);
                setting.setBottomMargin(1.5);
                setting.setLeftMargin(2);
                setting.setRightMargin(1.5);
                setting.setHeaderMargin(1.0);
                setting.setFooterMargin(1.0);
                setReport(sheet, ls);
                wb.write();
                wb.close();
                //Runtime.getRuntime().exec(new String[]{"rundll32","url.dll,FileProtocolHandler",fileName.getAbsolutePath()});
                Runtime.getRuntime().exec(new String[]{"libreoffice.calc",fileName.getAbsolutePath()});
        //create new sheet Excel
           }else
               JOptionPane.showMessageDialog(this, com.getRb().getString("selectVillage"),"Error",JOptionPane.ERROR_MESSAGE);
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }//GEN-LAST:event_btnExcelActionPerformed

    private void txtQureyCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtQureyCaretUpdate
        String sql;
        if(!txtQurey.getText().equals("")){
            sql = "select * from tblguest where name like '"+txtQurey.getText()+"'";
        }else
            sql = "select * from tblguest";
            getDatabase(sql, modelQuery);
    }//GEN-LAST:event_txtQureyCaretUpdate

    private void cmbVillageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbVillageActionPerformed
        String sql,village;
        village = cmbVillage.getSelectedItem().toString();
        if(village.isBlank())
            sql = "select * from tblguest";
        else
            sql = "select * from tblguest where village = '"+village.trim()+"'";
        ls = getDatabase(sql, modelVillage);
    }//GEN-LAST:event_cmbVillageActionPerformed
    private void setReport(WritableSheet sheet,List<Guest> ls)throws Exception{
        WritableFont fHeader = new WritableFont(WritableFont.createFont("Khmer OS Battambang"),14,WritableFont.BOLD,false,jxl.format.UnderlineStyle.NO_UNDERLINE,Colour.BLACK);
        WritableFont fTitle = new WritableFont(WritableFont.createFont("Khmer OS Battambang"),12,WritableFont.BOLD,false,jxl.format.UnderlineStyle.SINGLE,Colour.BLACK);
        WritableFont fWrite = new WritableFont(WritableFont.createFont("Khmer OS Battambang"),12);
        //set Title
        WritableCellFormat title = new WritableCellFormat(fTitle);
        title.setAlignment(Alignment.CENTRE);
        title.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        title.setBackground(Colour.GREEN);//Color(237,125,49)
        //set header
        WritableCellFormat header = new WritableCellFormat(fHeader);
        header.setAlignment(Alignment.CENTRE);
        header.setVerticalAlignment(VerticalAlignment.CENTRE);
        header.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
        header.setBackground(Colour.LIGHT_ORANGE);
        //set cell
        WritableCellFormat input = new WritableCellFormat(fWrite);
        input.setAlignment(Alignment.LEFT);
        input.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        input.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
        sheet.mergeCells(1, 0, 8, 1);
        sheet.setRowView(0, 700);
        sheet.addCell(new Label(1, 0, "ភូមិ"+ls.get(1).getVillage(), title));
            //SET FILED
        int row = 3;
        sheet.mergeCells(1, row, 1, row);//(C,R,C,R)
        sheet.setColumnView(1, 12);//(C,WIDTH)
        sheet.addCell(new Label(1,row,"ល.រ", header));
            
        sheet.mergeCells(2, row, 2, row);
        sheet.setColumnView(2, 18);
        sheet.addCell(new Label(2,row,"ឈ្មោះ", header));
            
        sheet.mergeCells(3, row, 3, row);
        sheet.setColumnView(3, 12);
        sheet.addCell(new Label(3,row,"ភេទ", header));
            
        sheet.mergeCells(4, row, 4, row);
        sheet.setColumnView(4, 18);
        sheet.addCell(new Label(4,row,"ភូមិ", header));
            
        sheet.mergeCells(5, row, 5, row);
        sheet.setColumnView(5, 18);
        sheet.addCell(new Label(5,row,"សង្កាត់", header));
            
        sheet.mergeCells(6, row, 6, row);
        sheet.setColumnView(6, 18);
        sheet.addCell(new Label(6,row,"ក្រុង/ស្រុក", header));
            
        sheet.mergeCells(7, row, 7, row);
        sheet.setColumnView(7, 18);
        sheet.addCell(new Label(7,row,"ខេត្ត", header));
            
        sheet.mergeCells(8, row, 8, row);
        sheet.setColumnView(8, 18);
        sheet.addCell(new Label(8,row,"ផ្សេងៗ", header));
        int rowCount = 4;
        int item = 0;
        int id = 1;
        int rowHeigth = 20;
        for(int i=0; i<ls.size(); i++){
            sheet.mergeCells(1, rowCount, 1, rowCount);//(C,R,C,R)
            sheet.setColumnView(1, rowHeigth);//(C,WIDTH)
            sheet.addCell(new Label(1,rowCount,id+"", input));
                
            sheet.mergeCells(2, rowCount, 2, rowCount);
            sheet.setColumnView(2, rowHeigth);
            sheet.addCell(new Label(2,rowCount,ls.get(i).getName(), input));
                
            sheet.mergeCells(3, rowCount, 3, rowCount);
            sheet.setColumnView(3, rowHeigth);
            sheet.addCell(new Label(3,rowCount,ls.get(i).getGender(), input));
                
            sheet.mergeCells(4, rowCount, 4, rowCount);
            sheet.setColumnView(4, rowHeigth);
            sheet.addCell(new Label(4,rowCount,ls.get(i).getVillage(), input));
                
            sheet.mergeCells(5, rowCount, 5, rowCount);
            sheet.setColumnView(5, rowHeigth);
            sheet.addCell(new Label(5,rowCount,ls.get(i).getComminue(), input));
                
            sheet.mergeCells(6, rowCount, 6, rowCount);
            sheet.setColumnView(6, rowHeigth);
            sheet.addCell(new Label(6,rowCount,ls.get(i).getCity(), input));
                
            sheet.mergeCells(7, rowCount, 7, rowCount);
            sheet.setColumnView(7, rowHeigth);
            sheet.addCell(new Label(7,rowCount,ls.get(i).getProvince(), input));
                
            sheet.mergeCells(8, rowCount, 8, rowCount);
            sheet.setColumnView(8, rowHeigth);
            sheet.addCell(new Label(8,rowCount,ls.get(i).getOther(), input));
            id++;
            rowCount++;
            item++;
        }
        sheet.addCell(new Label(8, rowCount+1, "ចំនួនសរុប ​"+item, header));
        //sheet.
    }
    private List<Guest> getDatabase(String sql,SubDefaultTableModel model){
        List<Guest> ls = new ArrayList<>();
        try{
            Statement s = ConnectionDB.getDataCon().createStatement(1004, 1008);
            ResultSet r = s.executeQuery(sql);
            model.removeAllRows();
            int no = 1;
            if(r.first())
              do{
                  ls.add(new Guest(r.getInt(1),r.getString(2),r.getString(3),r.getString(4),r.getString(5),r.getString(6)
                                    ,r.getString(7),r.getString(8)));
                  model.addRow(new Object[]{no,r.getInt(1),r.getString(2),r.getString(3),r.getString(4),r.getString(5),r.getString(6)
                                    ,r.getString(7),r.getString(8)});
                  no++;
                }while(r.next());
            r.close();
            s.close();        
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
        return ls;
    }
    private void getVillage(){
        try {
            String sql = "select distinct village from tblguest;";
            Statement s = ConnectionDB.getDataCon().createStatement(1004, 1008);
            ResultSet rs = s.executeQuery(sql);
            cmbVillage.addItem("");
            while(rs.next())
                cmbVillage.addItem(rs.getString(1));
            rs.close();
            s.close();
        } catch (Exception e) {
        }
    }
    private void setHeader(DefaultTableModel model,SubJTable tbl){
        model.addColumn(com.getRb().getString("lbNo"));
        model.addColumn(com.getRb().getString("lbGuestId"));
        model.addColumn(com.getRb().getString("lbName"));
        model.addColumn(com.getRb().getString("lbGender"));
        model.addColumn(com.getRb().getString("lbVillage"));
        model.addColumn(com.getRb().getString("lbCommune"));
        model.addColumn(com.getRb().getString("lbCity"));
        model.addColumn(com.getRb().getString("lbProvince"));
        model.addColumn(com.getRb().getString("lbOther"));
        tbl.setFontHeader(new Font("Khmer OS Battambang",Font.BOLD,15));
        tbl.setModel(model);
    }
    private String path;
    private List<Guest> ls;
    private SubDefaultTableModel modelGeneral,modelQuery,modelVillage; 
    private Component com;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExcel;
    private javax.swing.JComboBox<String> cmbVillage;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbGuestInforTitle;
    private javax.swing.JLabel lbSearchName;
    private javax.swing.JLabel lbSearchNameQ;
    private javax.swing.JLabel lbSearchVillage;
    private javax.swing.JPanel pGeneral;
    private javax.swing.JPanel pQuery;
    private javax.swing.JPanel pVillage;
    private javax.swing.JTabbedPane tab;
    private PlugIn.SubJTable tblGuest;
    private PlugIn.SubJTable tblQuery;
    private PlugIn.SubJTable tblVillage;
    private javax.swing.JTextField txtQurey;
    private javax.swing.JTextField txtSearchName;
    // End of variables declaration//GEN-END:variables

    
}


package resources;

/**
 *
 * @author CHINDA
 */
public abstract class Person implements Data{
    public String getComminue() {
        return comminue;
    }
    public void setComminue(String comminue) {
        this.comminue = comminue;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getProvince() {
        return province;
    }
    public void setProvince(String province) {
        this.province = province;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getVillage() {
        return village;
    }
    public void setVillage(String village) {
        this.village = village;
    }
    public Person() {
    }
    public Person(int id, String name, String gender, String village,String comminue,String city,String province) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.village = village;
        this.comminue = comminue;
        this.city = city;
        this.province = province;
    }
    public Person(int id,String name,String gender,String village){
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.village = village;
    }
    @Override
    public String toString() {
        return id+"\t"+name+"\t"+gender+"\t"+village+"\t"+comminue+"\t"+city+"\t"+province;
    }
    private int id;
    private String name;
    private String gender;
    private String village;
    private String comminue;
    private String city;
    private String province;
}

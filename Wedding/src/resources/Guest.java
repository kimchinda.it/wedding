
package resources;
import java.sql.*;
import Connection.ConnectionDB;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author CHINDA
 */
public class Guest<Guest> extends Person{

    /**
     * @return the fId
     */
    public int getfId() {
        return fId;
    }

    /**
     * @param fId the fId to set
     */
    public void setfId(int fId) {
        this.fId = fId;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the reil
     */
    public double getReil() {
        return reil;
    }

    /**
     * @param reil the reil to set
     */
    public void setReil(double reil) {
        this.reil = reil;
    }

    /**
     * @return the dollar
     */
    public double getDollar() {
        return dollar;
    }

    /**
     * @param dollar the dollar to set
     */
    public void setDollar(double dollar) {
        this.dollar = dollar;
    }

    /**
     * @return the dong
     */
    public double getDong() {
        return dong;
    }

    /**
     * @param dong the dong to set
     */
    public void setDong(double dong) {
        this.dong = dong;
    }

    /**
     * @return the other
     */
    public String getOther() {
        return other;
    }

    /**
     * @param other the other to set
     */
    public void setOther(String other) {
        this.other = other;
    }
    public Guest() {
    }
    public Guest( int id, String name, String gender, String village, String comminue, String city, String province,String other) {
        super(id, name, gender, village, comminue, city, province);
        this.other = other;
    }
    public Guest( int id, String name, String gender, String village, double reil, double dollar, double dong,String date,int fid ,String other) {
        super(id, name, gender, village);
        this.date = date;
        this.reil = reil;
        this.dollar = dollar;
        this.dong = dong;
        this.date = date;
        this.other = other;
        this.fId = fid;
    }
    public Guest( int id, String name,String gender,String village,double reil, double dollar, double dong,String date,String other) {
        super(id, name, gender, village);
        this.date = date;
        this.reil = reil;
        this.dollar = dollar;
        this.dong = dong;
        this.date = date;
        this.other = other;
    }
    private int fId;
    private String date;
    private double reil;
    private double dollar;
    private double dong;
    private String other;

    @Override
    public void insert() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     * @throws Exception
     */
    @Override
    public Guest select() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
}

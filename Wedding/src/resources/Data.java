
package resources;

/**
 *
 * @author chinda
 */
public interface Data<E> {
    void insert()throws Exception;
    void update(int id)throws Exception;
    void delete(int id)throws Exception;
    E select()throws Exception;
}


package resources;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author chinda
 */
public class Component {

    /**
     * @return the rb
     */
    public ResourceBundle getRb() {
        return rb;
    }

    public String getLanguage() {
        return language;
    }
    public void setLanguage(String language) {
        this.language = language;
        write(fileName.getAbsolutePath());
        
    }
    public void setLable(String[] text,JLabel ...lb){
        for(int i=0; i<lb.length; i++){
            lb[i].setText(rb.getString(text[i]));
        }
    }
    public void setButton(String[] text,JButton ...btn){
        for(int i=0; i<btn.length; i++)
            btn[i].setText(rb.getString(text[i]));
    }
    public void setPanel(String []text,JPanel ...panel){
        for(int i=0; i<panel.length; i++)
            panel[i].setBorder(javax.swing.BorderFactory.createTitledBorder(null, rb.getString(text[i]), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Khmer OS Battambang", 1, 12))); 
    }
    public Component() {
        read();
    }
    private void read(){
        try {
            FileInputStream fis = new FileInputStream(fileName);
            BufferedInputStream bis = new BufferedInputStream(fis);
            Scanner sc = new Scanner(bis);
            while(sc.hasNext()){
                setLanguage(sc.nextLine());
            }
            sc.close();
            bis.close(); 
            fis.close();
            if(language.equalsIgnoreCase("Khmer"))
            Locale.setDefault(new Locale("kh", "CM"));
            else
                Locale.setDefault(new Locale("en", "US"));
            rb = ResourceBundle.getBundle("msg");
        } catch (Exception e) {
            try {
                write(fileName.getAbsolutePath());
                read();
            } catch (Exception ex) {
            }
            
        }
    }
    private void write(String file){
        try {
            FileOutputStream fos = new FileOutputStream(new File(file));
            BufferedOutputStream bo = new BufferedOutputStream(fos);
            PrintStream ps = new PrintStream(bo);
            ps.print(language);
            ps.close();
            bo.close();
            fos.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public File fileName= new File("language.txt");
    private String language = "Khmer";
    private ResourceBundle rb = ResourceBundle.getBundle("msg");
    
    
}

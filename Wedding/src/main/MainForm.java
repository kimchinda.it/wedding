  
package main;
import Connection.ConnectionDB;
import com.formdev.flatlaf.FlatLightLaf;
import panel.InforGuest;
import panel.InforReturnMoney;
import panel.ReturMoney;
import panel.AddGuestDetail;
import panel.AddGuest;
import panel.InforGuestDetail;
import database.GuestDB;
import database.StoreDB;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import panel.Setting;
import resources.Component;
import resources.Guest;
import resources.Temp;

/**
 *
 * @author CHINDA
 */
public class MainForm extends javax.swing.JFrame {
    private Component com;
    public MainForm() {
        initComponents();
        
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pNavigation = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        pback = new javax.swing.JPanel();
        lbNavigate = new javax.swing.JLabel();
        lbAddGuest = new javax.swing.JLabel();
        lbAddMoney = new javax.swing.JLabel();
        lbReturnMoney = new javax.swing.JLabel();
        lbShowInfo = new javax.swing.JLabel();
        pShow = new javax.swing.JPanel();
        lbShowInfoGuest = new javax.swing.JLabel();
        lbShowInfoGuestDetail = new javax.swing.JLabel();
        lbShowInfoMoney = new javax.swing.JLabel();
        lbLogOut = new javax.swing.JLabel();
        lbName = new javax.swing.JLabel();
        lbSetting = new javax.swing.JLabel();
        pMain = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Wedding");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        pNavigation.setBackground(new java.awt.Color(0, 48, 84));
        pNavigation.setPreferredSize(new java.awt.Dimension(230, 588));
        pNavigation.setRequestFocusEnabled(false);

        pback.setBackground(new java.awt.Color(0, 48, 84));
        pback.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pbackMouseClicked(evt);
            }
        });
        pback.setLayout(null);

        lbNavigate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/next_x24.png"))); // NOI18N
        lbNavigate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbNavigateMouseClicked(evt);
            }
        });
        pback.add(lbNavigate);
        lbNavigate.setBounds(180, 6, 30, 24);

        lbAddGuest.setFont(new java.awt.Font("Khmer OS Battambang", 1, 15)); // NOI18N
        lbAddGuest.setForeground(new java.awt.Color(255, 255, 255));
        lbAddGuest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/add_user_group_woman_man_24px.png"))); // NOI18N
        lbAddGuest.setText("បន្ថែមពត៌មានភ្ញៀវ");
        lbAddGuest.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbAddGuest.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbAddGuestMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbAddGuestMouseEntered(evt);
            }
        });

        lbAddMoney.setFont(new java.awt.Font("Khmer OS Battambang", 1, 15)); // NOI18N
        lbAddMoney.setForeground(new java.awt.Color(255, 255, 255));
        lbAddMoney.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Color_Userx24.png"))); // NOI18N
        lbAddMoney.setText("កត់ត្រាចំណងដៃ");
        lbAddMoney.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbAddMoney.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbAddMoneyMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbAddMoneyMouseEntered(evt);
            }
        });

        lbReturnMoney.setFont(new java.awt.Font("Khmer OS Battambang", 1, 15)); // NOI18N
        lbReturnMoney.setForeground(new java.awt.Color(255, 255, 255));
        lbReturnMoney.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/money_circulation_24px.png"))); // NOI18N
        lbReturnMoney.setText("សងចំណងដៃ");
        lbReturnMoney.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbReturnMoney.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbReturnMoneyMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbReturnMoneyMouseEntered(evt);
            }
        });

        lbShowInfo.setFont(new java.awt.Font("Khmer OS Battambang", 1, 15)); // NOI18N
        lbShowInfo.setForeground(new java.awt.Color(255, 255, 255));
        lbShowInfo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/minus_24px.png"))); // NOI18N
        lbShowInfo.setText("បង្ហាញពត៌មាន");
        lbShowInfo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbShowInfo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbShowInfoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbShowInfoMouseEntered(evt);
            }
        });

        pShow.setBackground(new java.awt.Color(0, 48, 84));

        lbShowInfoGuest.setFont(new java.awt.Font("Khmer OS Battambang", 1, 15)); // NOI18N
        lbShowInfoGuest.setForeground(new java.awt.Color(255, 255, 255));
        lbShowInfoGuest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/increase1_24px.png"))); // NOI18N
        lbShowInfoGuest.setText("ពត៌មានភ្ញៀវ");
        lbShowInfoGuest.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbShowInfoGuest.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbShowInfoGuestMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbShowInfoGuestMouseEntered(evt);
            }
        });

        lbShowInfoGuestDetail.setFont(new java.awt.Font("Khmer OS Battambang", 1, 15)); // NOI18N
        lbShowInfoGuestDetail.setForeground(new java.awt.Color(255, 255, 255));
        lbShowInfoGuestDetail.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/increase_24px.png"))); // NOI18N
        lbShowInfoGuestDetail.setText("​ពត៌មានចំណងដៃ");
        lbShowInfoGuestDetail.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbShowInfoGuestDetail.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbShowInfoGuestDetailMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbShowInfoGuestDetailMouseEntered(evt);
            }
        });

        lbShowInfoMoney.setFont(new java.awt.Font("Khmer OS Battambang", 1, 15)); // NOI18N
        lbShowInfoMoney.setForeground(new java.awt.Color(255, 255, 255));
        lbShowInfoMoney.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/money_bag_24px.png"))); // NOI18N
        lbShowInfoMoney.setText("ពត៌មានការសងចំណងដៃ");
        lbShowInfoMoney.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbShowInfoMoney.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbShowInfoMoneyMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbShowInfoMoneyMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout pShowLayout = new javax.swing.GroupLayout(pShow);
        pShow.setLayout(pShowLayout);
        pShowLayout.setHorizontalGroup(
            pShowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pShowLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(pShowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbShowInfoGuestDetail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pShowLayout.createSequentialGroup()
                        .addComponent(lbShowInfoGuest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(lbShowInfoMoney, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)))
        );
        pShowLayout.setVerticalGroup(
            pShowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pShowLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(lbShowInfoGuest)
                .addGap(12, 12, 12)
                .addComponent(lbShowInfoGuestDetail)
                .addGap(12, 12, 12)
                .addComponent(lbShowInfoMoney)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        lbLogOut.setFont(new java.awt.Font("Khmer OS Battambang", 1, 15)); // NOI18N
        lbLogOut.setForeground(new java.awt.Color(255, 255, 255));
        lbLogOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/minus_24px.png"))); // NOI18N
        lbLogOut.setText("ចាកចេញ");
        lbLogOut.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbLogOut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbLogOutMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbLogOutMouseEntered(evt);
            }
        });

        lbName.setFont(new java.awt.Font("Khmer OS Battambang", 1, 14)); // NOI18N
        lbName.setForeground(new java.awt.Color(255, 255, 255));
        lbName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbName.setText("Hi");

        lbSetting.setFont(new java.awt.Font("Khmer OS Battambang", 1, 15)); // NOI18N
        lbSetting.setForeground(new java.awt.Color(255, 255, 255));
        lbSetting.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/minus_24px.png"))); // NOI18N
        lbSetting.setText("ការកំណត់");
        lbSetting.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbSetting.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbSettingMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbSettingMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout pNavigationLayout = new javax.swing.GroupLayout(pNavigation);
        pNavigation.setLayout(pNavigationLayout);
        pNavigationLayout.setHorizontalGroup(
            pNavigationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pback, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pNavigationLayout.createSequentialGroup()
                .addGroup(pNavigationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addComponent(lbAddGuest, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbAddMoney, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbReturnMoney, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbShowInfo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pShow, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbLogOut, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pNavigationLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(lbSetting, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pNavigationLayout.setVerticalGroup(
            pNavigationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pNavigationLayout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(lbName)
                .addGap(1, 1, 1)
                .addComponent(pback, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbAddGuest)
                .addGap(12, 12, 12)
                .addComponent(lbAddMoney)
                .addGap(12, 12, 12)
                .addComponent(lbReturnMoney)
                .addGap(12, 12, 12)
                .addComponent(lbShowInfo)
                .addGap(12, 12, 12)
                .addComponent(pShow, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbLogOut)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbSetting)
                .addContainerGap())
        );

        getContentPane().add(pNavigation, java.awt.BorderLayout.LINE_START);

        pMain.setBackground(java.awt.Color.white);
        pMain.setLayout(new java.awt.BorderLayout());
        getContentPane().add(pMain, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        //ls = new GuestDB();
        listLable = Arrays.asList(lbAddGuest,lbAddMoney,lbReturnMoney,lbShowInfo,lbShowInfoGuest,lbShowInfoGuestDetail,lbShowInfoMoney,lbLogOut,lbSetting);
        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(ds);
        com = new Component();
        String text[] = {"lbAddGuest","lbAddMoney","lbReturnMoney","lbShowInfo","lbLogOut","lbShowInfoGuest","lbShowInfoGuestDetail","lbShowInfoMoney","lbSetting"};
        com.setLable(text, lbAddGuest,lbAddMoney,lbReturnMoney,lbShowInfo,lbLogOut,
                lbShowInfoGuest,lbShowInfoGuestDetail,lbShowInfoMoney,lbSetting);
        lbName.setText("Hi "+ConnectionDB.getUserName());
        
    }//GEN-LAST:event_formWindowOpened

    private void lbAddGuestMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbAddGuestMouseEntered
        mouseHover(0, listLable);
    }//GEN-LAST:event_lbAddGuestMouseEntered

    private void lbAddMoneyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbAddMoneyMouseEntered
        mouseHover(1, listLable);
    }//GEN-LAST:event_lbAddMoneyMouseEntered

    private void lbReturnMoneyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbReturnMoneyMouseEntered
        mouseHover(2, listLable);
    }//GEN-LAST:event_lbReturnMoneyMouseEntered

    private void lbShowInfoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbShowInfoMouseEntered
        mouseHover(3, listLable);
    }//GEN-LAST:event_lbShowInfoMouseEntered

    private void lbShowInfoGuestMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbShowInfoGuestMouseEntered
        mouseHover(4, listLable);
    }//GEN-LAST:event_lbShowInfoGuestMouseEntered

    private void lbShowInfoGuestDetailMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbShowInfoGuestDetailMouseEntered
        mouseHover(5, listLable);
    }//GEN-LAST:event_lbShowInfoGuestDetailMouseEntered

    private void lbShowInfoMoneyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbShowInfoMoneyMouseEntered
        mouseHover(6, listLable);
    }//GEN-LAST:event_lbShowInfoMoneyMouseEntered

    private void lbNavigateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbNavigateMouseClicked
        if(navigate == true){
            pNavigation.setPreferredSize(new Dimension(30,this.getHeight()));
            lbNavigate.setIcon(new ImageIcon(getClass().getResource("/icon/previous_x24.png")));
            SwingUtilities.updateComponentTreeUI(this);
            lbNavigate.setBounds(1, 6, 30, lbNavigate.getHeight());
            navigate = false;
        }
        else{
            pNavigation.setPreferredSize(new Dimension(220,this.getHeight()));
            lbNavigate.setIcon(new ImageIcon(getClass().getResource("/icon/next_x24.png")));
            SwingUtilities.updateComponentTreeUI(this);
            lbNavigate.setBounds(180, 6, 30, lbNavigate.getHeight());
            navigate = true;
        }
    }//GEN-LAST:event_lbNavigateMouseClicked

    private void lbShowInfoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbShowInfoMouseClicked
        if(show == true){
            this.pShow.setPreferredSize(new Dimension(212, 138));
            lbShowInfo.setIcon(new ImageIcon(getClass().getResource("/icon/minus_24px.png")));
            show = false;
        }else{
           this.pShow.setPreferredSize(new Dimension(0,0));
            lbShowInfo.setIcon(new ImageIcon(getClass().getResource("/icon/plus_x24.png")));
            show = true;
        }
        SwingUtilities.updateComponentTreeUI(this);
    }//GEN-LAST:event_lbShowInfoMouseClicked

    private void lbAddGuestMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbAddGuestMouseClicked
        addPanel(new AddGuest());
        
    }//GEN-LAST:event_lbAddGuestMouseClicked

    private void lbAddMoneyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbAddMoneyMouseClicked
        addPanel(new AddGuestDetail());
    }//GEN-LAST:event_lbAddMoneyMouseClicked

    private void lbShowInfoGuestMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbShowInfoGuestMouseClicked
        addPanel(new InforGuest());
    }//GEN-LAST:event_lbShowInfoGuestMouseClicked

    private void lbShowInfoGuestDetailMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbShowInfoGuestDetailMouseClicked
        addPanel(new InforGuestDetail());
    }//GEN-LAST:event_lbShowInfoGuestDetailMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try {
            ConnectionDB.getDataCon().close();
        } catch (SQLException ex) {
            Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_formWindowClosing

    private void pbackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pbackMouseClicked
        lbNavigateMouseClicked(evt);
    }//GEN-LAST:event_pbackMouseClicked

    private void lbReturnMoneyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbReturnMoneyMouseClicked
        addPanel(new ReturMoney());
    }//GEN-LAST:event_lbReturnMoneyMouseClicked

    private void lbShowInfoMoneyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbShowInfoMoneyMouseClicked
        addPanel(new InforReturnMoney());
    }//GEN-LAST:event_lbShowInfoMoneyMouseClicked

    private void lbLogOutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbLogOutMouseClicked
        int click = JOptionPane.showConfirmDialog(this, com.getRb().getString("msgLogOut")+" "
                + ""+ConnectionDB.getUserName()+"?", com.getRb().getString("lbLogOut"), JOptionPane.YES_NO_OPTION);
        if(click == JOptionPane.YES_OPTION){
            this.dispose();
            LogIn.main(null);
            try {
                ConnectionDB.getDataCon().close();
            } catch (SQLException ex) {
                Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_lbLogOutMouseClicked

    private void lbLogOutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbLogOutMouseEntered
        mouseHover(7, listLable);
    }//GEN-LAST:event_lbLogOutMouseEntered

    private void lbSettingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbSettingMouseClicked
        Temp.THIS = this;
        addPanel(new Setting());
    }//GEN-LAST:event_lbSettingMouseClicked

    private void lbSettingMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbSettingMouseEntered
        mouseHover(8, listLable);
    }//GEN-LAST:event_lbSettingMouseEntered
    private void mouseHover(int index,List<JLabel> ls){
        for(int i=0; i<ls.size(); i++){
            if(i==index){
                ls.get(i).setForeground(Color.red);
                ls.get(i).setFont(new Font("Khmer OS Battambang", Font.BOLD, 16));
            }else{
                ls.get(i).setForeground(new Color(214,217,223));
                ls.get(i).setFont(new Font("Khmer OS Battambang", Font.BOLD, 15));
            }
        }
    }
    private void addPanel(JPanel panel){
        pMain.removeAll();
        pMain.add(panel,java.awt.BorderLayout.CENTER);
        pMain.validate();
        panel.setVisible(true);
    }
    //private StoreDB<Guest> ls,guestDetial;
    private List<JLabel> listLable;
    private boolean navigate = true;
    private boolean show = false;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            UIManager.setLookAndFeel(new FlatLightLaf());
        } catch (Exception e) {
        }
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbAddGuest;
    private javax.swing.JLabel lbAddMoney;
    private javax.swing.JLabel lbLogOut;
    private javax.swing.JLabel lbName;
    private javax.swing.JLabel lbNavigate;
    private javax.swing.JLabel lbReturnMoney;
    private javax.swing.JLabel lbSetting;
    private javax.swing.JLabel lbShowInfo;
    private javax.swing.JLabel lbShowInfoGuest;
    private javax.swing.JLabel lbShowInfoGuestDetail;
    private javax.swing.JLabel lbShowInfoMoney;
    private javax.swing.JPanel pMain;
    private javax.swing.JPanel pNavigation;
    private javax.swing.JPanel pShow;
    private javax.swing.JPanel pback;
    // End of variables declaration//GEN-END:variables
}

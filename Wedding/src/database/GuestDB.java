
package database;

import java.util.ArrayList;
import java.util.List;
import resources.Guest;
import java.sql.*;
import Connection.*;
/**
 *
 * @author CHINDA
 */
public class GuestDB implements StoreDB<Guest> {
    private  List<Guest> ls = new ArrayList<>();
    public List<Guest> getLs() {
        return ls;
    }
    public void setLs(List<Guest> ls) {
        this.ls = ls;
    }    
    @Override
    public int size() {
        return getLs().size();
    }
    @Override
    public Guest get(int index) {
        return getLs().get(index);
    }
    public Guest getTemp(int index) {
        return getLs().get(index);
    }
    @Override
    public void add(Guest temp){
        getLs().add(temp);
    }
    @Override
    public void set(int index, Guest object) throws Exception{
        
        getLs().set(index, object);
    }
    @Override
    public void remove(int index) throws Exception{
   
        getLs().remove(index);
    }
    @Override
    public void removeAll() {
        for(int i=getLs().size()-1; i>=0; i--)
            getLs().remove(i);
    }
    @Override
    public List<Guest> getFilterName(String name) {
        List<Guest> arr = new ArrayList<>();
        if(name.equals(""))
            return getLs();
        else{
            String subName;
            for(int i=0; i<getLs().size(); i++){
                subName = getLs().get(i).getName();
                if(name.length()>subName.length())
                    continue;
                subName = subName.substring(0,name.length());
                if(name.equalsIgnoreCase(subName))
                    arr.add(getLs().get(i));
            }
        }
        return arr;
    }
    public List<Guest> getFilterVillage(String village){
        List<Guest> ls = new ArrayList<>();
        if(village.equals(""))
            return getLs();
        else{
            String subVillage;
            for(int i=0; i<getLs().size(); i++){
                subVillage = getLs().get(i).getVillage();
                if(village.length()>subVillage.length())
                    continue;
                subVillage = subVillage.substring(0,village.length());
                if(village.equalsIgnoreCase(subVillage))
                    ls.add(getLs().get(i));
            }
        }
        return ls;
    }
    @Override
    public List<Guest> getAll() {
        return ls;
    }

    public GuestDB() {
    }

    public void insertDataIntoDB() throws Exception {
        
    }
    @Override
    public int index(int id) {
        for(int i=0; i<ls.size(); i++)
            if(id == ls.get(i).getId()){
                return i;
            }
        return -1;
    }

    @Override
    public void setDatbase(String sql) throws Exception {
        Statement s = ConnectionDB.getDataCon().createStatement(1004, 1008);
        s.execute(sql);
    }

    @Override
    public List<Guest> getDatabase(String sql) throws Exception {
        Statement s = ConnectionDB.getDataCon().createStatement(1004, 1008);
        ResultSet r = s.executeQuery(sql);
        ResultSetMetaData rm = r.getMetaData();
        String header[] = new String[rm.getColumnCount()];
        String row[] = new String[header.length];
        if(r.first())
            do{
                for(int i=0; i<row.length; i++)
                    row[i] = r.getString(i+1);
            }while(r.next());
                r.close();
                s.close();
                return null;
    }
    
}


package database;

import java.util.List;

/**
 *
 * @author CHINDA
 * @param <E>
 */
public interface StoreDB<E>{
    //void initialize()throws Exception;
    int size();
    E get(int index);
    List<E> getAll();
    void add(E object) throws Exception;
    void set(int index, E e) throws Exception;
    void remove(int index) throws Exception;
    void removeAll();
    int index(int id);
    List<E> getFilterName(String name);
    void setDatbase(String sql)throws Exception;
    List<E> getDatabase(String sql) throws Exception;
}
